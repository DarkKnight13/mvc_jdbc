<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ page import="java.sql.*" %>


<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>



<%
	if( session.getAttribute("user") == null ){
		response.sendRedirect("error.jsp");
	}
 
	ResultSet rs = (ResultSet)request.getAttribute("resultSet"); 
%>

<div align="center" class="container">
	<h3>Search Results....</h3>   <br/>
	<table border="1" class="table-bordered">
	  <tr>
	    <th>Id</th>
	    <th>First Name</th>
	    <th>Last Name</th>
	  </tr>
	  <%
	  while( rs.next() ){
	  %>
	  <tr>
	  	<td><%= rs.getInt("id") %></td>
	  	<td><%= rs.getString("first_name") %></td>
	  	<td><%= rs.getString("last_name") %></td>
	  </tr>
	  <%
	  }
	  %>
	 
	</table>
</div>


    
    
    
    