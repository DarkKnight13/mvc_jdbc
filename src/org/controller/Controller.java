/*
 * TNS
 * 
 * mvC
 * Controller of the web application
 * 
 */



package org.controller;

import java.io.IOException;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.service.AddEmployeeService;
import org.service.LogInService;
import org.service.SearchEmployeeService;

import com.domain.Employee;

/**
 * Servlet implementation class Controller
 */


public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Controller() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    
    

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	
	
	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		
		
		String task = request.getParameter("act").toString();
		RequestDispatcher rd;
		
		Employee emp = new Employee();
		emp.setFirstName(request.getParameter("firstName"));
		emp.setLastName(request.getParameter("lastName"));
		
		System.out.println( emp.getFirstName() );
		System.out.println( emp.getLastName() );
		
		
		if( "Add Employee".equals( task ) ){
			AddEmployeeService aes = new AddEmployeeService();
			String flag = aes.saveEmp(emp);
			System.out.println( "Add Employee Flag: " + flag );
			rd = getServletContext().getRequestDispatcher("/added.jsp");
			rd.include(request, response);
			
		}
		else if( "Search Employees".equals(task)){
			SearchEmployeeService ses = new SearchEmployeeService();
			//Employee list[] = (Employee [])ses.search(emp);
			ResultSet rs = ses.search(emp);
			request.setAttribute("resultSet", rs );
			rd = getServletContext().getRequestDispatcher("/search.jsp");
			
			rd.include(request, response);
		}
		else if( "Login".equals(task) ){
			//Employee emp = new Employee();
			
			emp.setUserName(request.getParameter("userName"));
			emp.setPassword(request.getParameter("password"));
			
			LogInService lis = new LogInService();
			emp = lis.checkCreds( emp );
			
			if( emp != null ){
				HttpSession session = request.getSession();
				session.setAttribute("user", emp);
				response.sendRedirect("index.jsp");
			}
			else{
				response.sendRedirect("index.html");
			}
		}
		else{
			
		}
		
		
		
		
		
		
		
		
	}

	
	
	
}
