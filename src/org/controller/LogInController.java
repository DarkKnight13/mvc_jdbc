/* 
 * TNS
 * 
 * This Servlet is not used in the Project anywhere
 * 
 * 
 */



package org.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.service.LogInService;

import com.domain.Employee;

/**
 * Servlet implementation class LogInController
 */


public class LogInController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
	
	
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LogInController() {
        super();
        // TODO Auto-generated constructor stub
    }

    
    
    
    
    
    
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
	
	
	
	
	
	
	
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		Employee emp = new Employee();
		
		emp.setUserName(request.getParameter("userName"));
		emp.setPassword(request.getParameter("password"));
		
		LogInService lis = new LogInService();
		emp = lis.checkCreds( emp );
		
		if( emp != null ){
			HttpSession session = request.getSession();
			session.setAttribute("user", emp);
			response.sendRedirect("index.jsp");
		}
		else{
			response.sendRedirect("index.html");
		}
		
	}

}
